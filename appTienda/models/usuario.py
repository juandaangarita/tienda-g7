from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UsuarioManager(BaseUserManager):
    def crear_usuario(self, useremail, password=None):
        """
        Se crea y guarda un usuario con el email y la contraseña dada.
        """
        if not useremail:
            raise ValueError('Las usuarios deben tener un nombre de usuario')
        usuario = self.model(useremail=useremail)
        usuario.set_password(password)
        usuario.save(using=self._db)
        return usuario

    def create_superuser(self, useremail, password):
        """
        Crea y guarda un superusuario con el nombre de usuario y la contraseña dados.
        """
        usuario = self.crear_usuario(
            useremail=useremail,
            password=password,
        )
        usuario.is_admin = True
        usuario.save(using=self._db)
        return usuario

class Usuario(AbstractBaseUser, PermissionsMixin):
    usu_nombre = models.CharField('Usu_nombre', max_length = 30)
    usu_apellido = models.CharField('Usu_apellido', max_length = 30)
    usu_email = models.EmailField('Usu_email', max_length = 100, primary_key=True)
    usu_password = models.CharField('Usu_password', max_length = 256)
    usu_celular = models.IntegerField('Usu_celular', max_length = 30)
    

    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN' 
        self.usu_password = make_password(self.usu_password, some_salt)
        super().save(**kwargs)

    objects = UsuarioManager()
    USERNAME_FIELD = 'useremail'
