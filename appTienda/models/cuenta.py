from django.db import models

class Cuenta(models.Model):
    id_cuenta = models.AutoField(primary_key = True)
    usu_email = models.CharField('Correo electronico', max_length = 256)
