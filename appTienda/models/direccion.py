from django.db import models
from .cuenta import Cuenta

class Direccion(models.Model):
    id_direccion = models.AutoField(primary_key = True)
    id_cuenta = models.ForeignKey(Cuenta, related_name = 'direccion', on_delete = models.CASCADE)
    dir_completa = models.CharField('Dirección_Completa', max_length = 256)
