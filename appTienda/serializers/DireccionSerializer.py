from appTienda.models.direccion import Direccion
from rest_framework import serializers
class DireccionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Direccion
        fields = ['Direccion_Completa']