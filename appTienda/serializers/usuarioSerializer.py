from rest_framework import serializers
from appTienda.models.usuario import Usuario
from appTienda.models.cuenta import Cuenta
from appTienda.serializers.cuentaSerializer import CuentaSerializer

class UsuarioSerializer(serializers.ModelSerializer):
    cuenta = CuentaSerializer()
    class Meta:
        model = Usuario
        fields = ['usu_nombre', 'usu_apellido', 'usu_email', 'usu_password', 'usu_celular', 'cuenta']

    def create(self, validated_data):
        accountData = validated_data.pop('cuenta')
        userInstance = Usuario.objects.create(**validated_data)
        Cuenta.objects.create(user=userInstance, **accountData)
        return userInstance

    def to_representation(self, obj):
        usuario = Usuario.objects.get(id=obj.usu_email)
        cuenta = Cuenta.objects.get(user=obj.usu_email)       
        return {
                    'usu_nombre': usuario.usu_nombre, 
                    'usu_apellido': usuario.usu_apellido,
                    'usu_email': usuario.usu_email,
                    'usu_celular': usuario.usu_celular,
                    'cuenta': {
                        'usu_email': cuenta.usu_email,
                        'balance': cuenta.balance,
                        'lastChangeDate': cuenta.lastChangeDate,
                        'isActive': cuenta.isActive
                    }
                }