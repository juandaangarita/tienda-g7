from django.contrib import admin
from .models.cuenta import Cuenta
from .models.usuario import Usuario
from .models.pedido import Pedido
from .models.direccion import Direccion

admin.site.register(Cuenta)
admin.site.register(Usuario)
admin.site.register(Pedido)
admin.site.register(Direccion)
# Register your models here.
